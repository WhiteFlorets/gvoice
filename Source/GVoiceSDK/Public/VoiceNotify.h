// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "VoiceNotify.generated.h"

using namespace gcloud_voice;

UENUM(BlueprintType)
enum class EGCloudVoiceCompleteCode : uint8
{
	/* join room succ */
	GV_ON_JOINROOM_SUCC = 1,
	/* join room timeout */
	GV_ON_JOINROOM_TIMEOUT,
	/* communication with svr occur some err, such as err data recv from svr */
	GV_ON_JOINROOM_SVR_ERR,
	/* reserved, our internal unknow err */
	GV_ON_JOINROOM_UNKNOWN,

	/* net err,may be can't connect to network */
	GV_ON_NET_ERR,

	/* quitroom succ, if you have join room succ first, quit room will alway return succ */
	GV_ON_QUITROOM_SUCC,

	/* apply message authkey succ */
	GV_ON_MESSAGE_KEY_APPLIED_SUCC,
	/* apply message authkey timeout */
	GV_ON_MESSAGE_KEY_APPLIED_TIMEOUT,
	/* communication with svr occur some err, such as err data recv from svr */
	GV_ON_MESSAGE_KEY_APPLIED_SVR_ERR,
	/* reserved,  our internal unknow err */
	GV_ON_MESSAGE_KEY_APPLIED_UNKNOWN,

	/* upload record file succ */
	GV_ON_UPLOAD_RECORD_DONE,
	/* upload record file occur error */
	GV_ON_UPLOAD_RECORD_ERROR,
	/* download record file succ */
	GV_ON_DOWNLOAD_RECORD_DONE,
	/* download record file occur error */
	GV_ON_DOWNLOAD_RECORD_ERROR,

	/* speech to text successful */
	GV_ON_STT_SUCC,
	/* speech to text with timeout */
	GV_ON_STT_TIMEOUT,
	/* server's error */
	GV_ON_STT_APIERR,

	/* speech to text successful */
	GV_ON_RSTT_SUCC,
	/* speech to text with timeout */
	GV_ON_RSTT_TIMEOUT,
	/* server's error */
	GV_ON_RSTT_APIERR,

	/* the record file played end */
	GV_ON_PLAYFILE_DONE,

	/* Dropped from the room */
	GV_ON_ROOM_OFFLINE,
	GV_ON_UNKNOWN,
	/* Change Role Success */
	GV_ON_ROLE_SUCC,
	/* Change Role with tiemout */
	GV_ON_ROLE_TIMEOUT,
	/* To much Anchor */
	GV_ON_ROLE_MAX_AHCHOR,
	/* The same role */
	GV_ON_ROLE_NO_CHANGE,
	/* server's error */
	GV_ON_ROLE_SVR_ERROR,

	/* need retry stt */
	GV_ON_RSTT_RETRY,
};

/**
 * 
 */
UCLASS()
class GVOICESDK_API UVoiceNotify : public UObject, public IGCloudVoiceNotify
{
	GENERATED_BODY()

public:
	virtual ~UVoiceNotify();
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnJoinRoom, EGCloudVoiceCompleteCode, code, FString, roomName, int32, memberID);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnQuitRoom, EGCloudVoiceCompleteCode, code, FString, roomName);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnMemberVoice, FString, roomName, int32, member, int32, status);

public:
	// Real-Time Callback
	/**
	 * Callback when JoinXxxRoom successful or failed.
	 *
	 * @param code : a GCloudVoiceCompleteCode code . You should check this first.
	 * @param roomName : name of your joining, should be 0-9A-Za-Z._- and less than 127 bytes
	 * @param memberID : if success, return the memberID
	 */
	virtual void OnJoinRoom(GCloudVoiceCompleteCode code, const char *roomName, int memberID) override;

	/**
	 * Callback when dropped from the room
	 *
	 * @param code : a GCloudVoiceCompleteCode code . You should check this first.
	 * @param roomName : name of your joining, should be 0-9A-Za-Z._- and less than 127 bytes
	 * @param memberID : if success, return the memberID
	 */
	virtual void OnStatusUpdate(GCloudVoiceCompleteCode status, const char *roomName, int memberID) override;

	/**
	 * Callback when QuitRoom successful or failed.
	 *
	 * @param code : a GCloudVoiceCompleteCode code . You should check this first.
	 * @param roomName : name of your joining, should be 0-9A-Za-Z._- and less than 127 bytes
	 */
	virtual void OnQuitRoom(GCloudVoiceCompleteCode code, const char *roomName) override;

	/**
	 ** Deprecate from GVoice 1.1.14
	 * Callback when someone saied or silence in the same room.
	 *
	 * @param count: count of members who's status has changed.
	 * @param members: a int array composed of [memberid_0, status,memberid_1, status ... memberid_2*count, status]
	 * here, status could be 0, 1, 2. 0 meets silence and 1/2 means saying
	 */
	virtual void OnMemberVoice(const unsigned int *members, int count) override;


	/**
	 * Callback when someone saied or silence in the same room.
	 *
	 * @param roomName: name of the room.
	 * @param member: the member's ID
	 * @param status : status could be 0, 1, 2. 0 meets silence and 1/2 means saying
	 */
	virtual void OnMemberVoice(const char *roomName, unsigned int member, int status) override;

	// Voice Message Callback
	/**
	 * Callback when upload voice file successful or failed.
	 *
	 * @param code: a GCloudVoiceCompleteCode code . You should check this first.
	 * @param filePath: file to upload
	 * @param fileID: if success ,get back the id for the file.
	 */
	virtual void OnUploadFile(GCloudVoiceCompleteCode code, const char *filePath, const char *fileID) override;

	/**
	 * Callback when download voice file successful or failed.
	 *
	 * @param code: a GCloudVoiceCompleteCode code . You should check this first.
	 * @param filePath: file to download to .
	 * @param fileID: if success ,get back the id for the file.
	 */
	virtual void OnDownloadFile(GCloudVoiceCompleteCode code, const char *filePath, const char *fileID) override;

	/**
	 * Callback when finish a voice file play end.
	 *
	 * @param code: a GCloudVoiceCompleteCode code . You should check this first.
	 * @param filePath: file had been plaied.
	 */
	virtual void OnPlayRecordedFile(GCloudVoiceCompleteCode code, const char *filePath) override;

	/**
	 * Callback when query message key successful or failed.
	 *
	 * @param code: a GCloudVoiceCompleteCode code . You should check this first.
	 */
	virtual void OnApplyMessageKey(GCloudVoiceCompleteCode code) override;

	// Translate
	/**
	 * Callback when translate voice to text successful or failed.
	 *
	 * @param code: a GCloudVoiceCompleteCode code . You should check this first.
	 * @param fileID : file to translate
	 * @param result : the destination text of the destination language.
	 */
	virtual void OnSpeechToText(GCloudVoiceCompleteCode code, const char *fileID, const char *result) override;

	/**
	 * Callback when client is using microphone recording audio
	 *
	 * @param pAudioData : audio data pointer
	 * @param nDataLength : audio data length
	 * @param result : void
	 */
	virtual void OnRecording(const unsigned char* pAudioData, unsigned int nDataLength) override;

	/**
	 * Callback when translate voice to text successful or failed.
	 */
	virtual void OnStreamSpeechToText(GCloudVoiceCompleteCode code, int error, const char *result, const char *voicePath) override;

	/**
	 * Callback when change to another role
	 */
	virtual void OnRoleChanged(GCloudVoiceCompleteCode code, const char *roomName, int memberID, int role) override;

	/**
	 * Get the notify instance (singleton object)
	 *
	 * @return UVoiceNotify pointer
	 */
	UFUNCTION(BlueprintPure, Category = "GVoice")
		static UVoiceNotify* GetNotifyInstance();

private:
	UVoiceNotify(const FObjectInitializer& ObjectInitializer);

private:
	// The UVoiceNotify instance pointer (singleton object)
	static UVoiceNotify* NotifyInstance;

	UPROPERTY(BlueprintAssignable, Category = "GVoice")
		FOnJoinRoom OnJoinRoomCompleted;

	UPROPERTY(BlueprintAssignable, Category = "GVoice")
		FOnQuitRoom OnQuitRoomCompleted;

	UPROPERTY(BlueprintAssignable, Category = "GVoice")
		FOnMemberVoice OnMemberVoiceCompleted;
};
