// Fill out your copyright notice in the Description page of Project Settings.

#include "GVoiceClient.h"
#include "VoiceNotify.h"

UGVoiceClient* UGVoiceClient::GVoiceClient = nullptr;

UGVoiceClient::~UGVoiceClient()
{
	//delete m_VoiceEngine;
}

void UGVoiceClient::Tick(float DeltaTime)
{
	m_VoiceEngine->Poll();
}

bool UGVoiceClient::IsTickable() const
{
	return true;
}

TStatId UGVoiceClient::GetStatId() const
{
	return TStatId();
}

UGVoiceClient::UGVoiceClient(const FObjectInitializer& ObjectInitializer)
	: UObject{ ObjectInitializer }
	, m_VoiceEngine{ gcloud_voice::GetVoiceEngine() }
{
}

void UGVoiceClient::Initializer()
{
	if (!(GVoiceClient && GVoiceClient->IsValidLowLevel()))
	{
		GVoiceClient = NewObject<UGVoiceClient>();
		GVoiceClient->AddToRoot();
	}
}

UGVoiceClient* UGVoiceClient::GetVoiceClient()
{
	Initializer();
	return GVoiceClient;
}

int32 UGVoiceClient::SetAppInfo(const FString& appID, const FString& appKey, const FString& OpenID)
{
	if (nullptr == m_VoiceEngine) return -1;

	return m_VoiceEngine->SetAppInfo(TCHAR_TO_ANSI(*appID), TCHAR_TO_ANSI(*appKey), TCHAR_TO_ANSI(*OpenID));
}

int32 UGVoiceClient::InitVoiceEngine()
{
	return m_VoiceEngine->Init();
}

int32 UGVoiceClient::SetMode(EVoiceMode VoiceMode)
{
	return m_VoiceEngine->SetMode(static_cast<gcloud_voice::IGCloudVoiceEngine::GCloudVoiceMode>(VoiceMode));
}

int32 UGVoiceClient::OnPause()
{
	return m_VoiceEngine->Pause();
}

int32 UGVoiceClient::OnResume()
{
	return m_VoiceEngine->Resume();
}

int32 UGVoiceClient::SetNotify(UVoiceNotify* NotifyInstance)
{
	if (!(NotifyInstance && NotifyInstance->IsValidLowLevel())) return -1;
	return m_VoiceEngine->SetNotify(NotifyInstance);
}

int32 UGVoiceClient::EnableMultiRoom(bool bEnable)
{
	return m_VoiceEngine->EnableMultiRoom(bEnable);
}

int32 UGVoiceClient::JoinTeamRoom(const FString& RoomName, int32 msTimeout)
{
	if (!JoinedRoomName.Contains(RoomName))
	{
		return m_VoiceEngine->JoinTeamRoom(TCHAR_TO_ANSI(*RoomName), msTimeout);
	}
	else
	{
		UE_LOG(LogTemp, Log, TEXT("Aleady joined Room : %s"), *RoomName);
	}
	return -1;
}

int32 UGVoiceClient::JoinNationalRoom(const FString& RoomName, EVoiceMemberRole MemberRole, int32 msTimeout)
{
	if (!JoinedRoomName.Contains(RoomName))
	{
		return m_VoiceEngine->JoinNationalRoom(TCHAR_TO_ANSI(*RoomName), static_cast<gcloud_voice::IGCloudVoiceEngine::GCloudVoiceMemberRole>(MemberRole), msTimeout);
	}
	else
	{
		UE_LOG(LogTemp, Log, TEXT("Aleady joined Room : %s"), *RoomName);
	}
	return -1;
}

int32 UGVoiceClient::QuitRoom(const FString& RoomName, int32 msTimeout)
{
	if (JoinedRoomName.Contains(RoomName))
	{
		return m_VoiceEngine->QuitRoom(TCHAR_TO_ANSI(*RoomName), msTimeout);
	}
	else
	{
		UE_LOG(LogTemp, Log, TEXT("QuitRoom doesn't join room : %s"), *RoomName);
	}
	return -1;
}

int32 UGVoiceClient::TestMic()
{
	return m_VoiceEngine->TestMic();
}

int32 UGVoiceClient::GetMicLevel()
{
	return m_VoiceEngine->GetMicLevel(true);
}

int32 UGVoiceClient::OpenMic()
{
	return m_VoiceEngine->OpenMic();
}

int32 UGVoiceClient::CloseMic()
{
	return m_VoiceEngine->CloseMic();
}

int32 UGVoiceClient::OpenSpeaker()
{
	return m_VoiceEngine->OpenSpeaker();
}

int32 UGVoiceClient::CloseSpeaker()
{
	return m_VoiceEngine->CloseSpeaker();
}

int32 UGVoiceClient::SetMicVolume(int vol)
{
	return m_VoiceEngine->SetMicVolume(vol);
}

int32 UGVoiceClient::SetSpeakerVolume(int vol)
{
	return m_VoiceEngine->SetSpeakerVolume(vol);
}

