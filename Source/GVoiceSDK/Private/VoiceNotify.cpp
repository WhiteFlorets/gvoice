// Fill out your copyright notice in the Description page of Project Settings.

#include "VoiceNotify.h"
#include "GVoiceClient.h"

UVoiceNotify* UVoiceNotify::NotifyInstance = nullptr;

UVoiceNotify::~UVoiceNotify()
{

}

void UVoiceNotify::OnJoinRoom(GCloudVoiceCompleteCode code, const char *roomName, int memberID)
{
	if (gcloud_voice::GV_ON_JOINROOM_SUCC == code)
	{
		UGVoiceClient::GetVoiceClient()->JoinedRoomName.AddUnique(FString(ANSI_TO_TCHAR(roomName)));
		UGVoiceClient::GetVoiceClient()->OpenMic();
		UGVoiceClient::GetVoiceClient()->OpenSpeaker();
		
		UGVoiceClient::GetVoiceClient()->SetMicVolume(1000);
		UGVoiceClient::GetVoiceClient()->SetSpeakerVolume(0xffff);
	}

	OnJoinRoomCompleted.Broadcast(static_cast<EGCloudVoiceCompleteCode>(code), FString(ANSI_TO_TCHAR(roomName)), memberID);
}

void UVoiceNotify::OnStatusUpdate(GCloudVoiceCompleteCode status, const char *roomName, int memberID)
{

}

void UVoiceNotify::OnQuitRoom(GCloudVoiceCompleteCode code, const char *roomName)
{
	if (gcloud_voice::GV_ON_QUITROOM_SUCC == code)
	{
		UGVoiceClient::GetVoiceClient()->JoinedRoomName.Remove(FString(ANSI_TO_TCHAR(roomName)));
		if (UGVoiceClient::GetVoiceClient()->JoinedRoomName.Num() == 0)
		{
			UGVoiceClient::GetVoiceClient()->CloseMic();
			UGVoiceClient::GetVoiceClient()->CloseSpeaker();
			UGVoiceClient::GetVoiceClient()->JoinedRoomName.Empty();
		}
	}

	OnQuitRoomCompleted.Broadcast(static_cast<EGCloudVoiceCompleteCode>(code), FString(ANSI_TO_TCHAR(roomName)));
}

void UVoiceNotify::OnMemberVoice(const unsigned int *members, int count)
{
	
}

void UVoiceNotify::OnMemberVoice(const char *roomName, unsigned int member, int status)
{
	OnMemberVoiceCompleted.Broadcast(FString(ANSI_TO_TCHAR(roomName)), member, status);
}

void UVoiceNotify::OnUploadFile(GCloudVoiceCompleteCode code, const char *filePath, const char *fileID)
{

}

void UVoiceNotify::OnDownloadFile(GCloudVoiceCompleteCode code, const char *filePath, const char *fileID)
{

}

void UVoiceNotify::OnPlayRecordedFile(GCloudVoiceCompleteCode code, const char *filePath)
{

}

void UVoiceNotify::OnApplyMessageKey(GCloudVoiceCompleteCode code)
{

}

void UVoiceNotify::OnSpeechToText(GCloudVoiceCompleteCode code, const char *fileID, const char *result)
{

}

void UVoiceNotify::OnRecording(const unsigned char* pAudioData, unsigned int nDataLength)
{

}

void UVoiceNotify::OnStreamSpeechToText(GCloudVoiceCompleteCode code, int error, const char *result, const char *voicePath)
{

}

void UVoiceNotify::OnRoleChanged(GCloudVoiceCompleteCode code, const char *roomName, int memberID, int role)
{

}

UVoiceNotify* UVoiceNotify::GetNotifyInstance()
{
	if (!(NotifyInstance && NotifyInstance->IsValidLowLevel()))
	{
		NotifyInstance = NewObject<UVoiceNotify>();
		NotifyInstance->AddToRoot();
	}
	return NotifyInstance;
}

UVoiceNotify::UVoiceNotify(const FObjectInitializer& ObjectInitializer)
	:UObject{ ObjectInitializer }
{

}
